import Dexie from 'dexie';

export const db = new Dexie('entradas');
db.version(2).stores({
  entradas: '++id, titulo, autor, contenido, publicacion'
});