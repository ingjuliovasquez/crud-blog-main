# Gestor de Contenido para Blog Sencillo
Este proyecto es un gestor de contenido para un blog sencillo desarrollado en Next.js con un backend en Node.js. Sigue las instrucciones a continuación para instalar y configurar la aplicación.

## Instrucciones de Instalación
1. Clonar el Repositorio

Clona el repositorio desde GitHub o descárgalo como un archivo ZIP.

```git clone https://ingjuliovasquez@gitlab.com/ingjuliovasquez/mongo_crud.git```

2. Instalar Dependencias
Navega hasta la carpeta raíz del proyecto y ejecuta el siguiente comando para instalar las dependencias utilizando npm:

```bash
cd mongo_crud
npm install
```

3. Configurar la Base de Datos MySQL

    * Asegúrate de que el servidor MySQL esté en funcionamiento y que tengas acceso a una base de datos.

    * Abre el archivo /configDB/db.js y modifica las variables de configuración según los datos de tu servidor de base de datos, como el nombre de la base de datos, el usuario y la contraseña.

4. Crear la Tabla de la Base de Datos

    * En el archivo ```/database/db.sql```, encontrarás los comandos SQL necesarios para crear la tabla de la base de datos que se utilizará para almacenar las entradas del blog. Ejecuta estos comandos en tu servidor de base de datos para configurar la estructura de la tabla.
    
    ```sql
    mysql -u tuusuario -p < /ruta/al/archivo/database/db.sql
    ```
    ```Configuración inicial de base de datos
    *host: 'localhost',
    *user: 'root',
    *password: '',
    *port: 3306,
    *database: 'blogtest'
    ```

    En el archivo ../configDB/db.js se encuentra la configuración, por si es necesario modificarla.
 

5. Ejecutar la Aplicación
Después de configurar la base de datos y asegurarte de que las dependencias estén instaladas, puedes ejecutar la aplicación.

```bash 
npm run dev
```

La aplicación estará disponible en ```http://localhost:3000```. Abre tu navegador y navega a esta dirección para acceder al gestor de contenido.

## Uso de la Aplicación
Una vez que la aplicación esté en funcionamiento, puedes visitar http://localhost:3000 para ver la lista de entradas de blog. Si deseas agregar una nueva entrada, simplemente haz clic en el botón correspondiente en la barra de navegación.