"use client";

import { HiOutlineTrash } from "react-icons/hi";
import { useRouter } from "next/navigation";
import controller from '@/database/controller'
import { db } from "@/configDB/indexedDb";
import updateDate from "@/functions/updateDate";

export default function RemoveBtn({ id }) {
  const router = useRouter();
  const removeTopic = async () => {
    const confirmed = confirm("Desea eliminar este registro?");

    if (confirmed) {
      const date = new Date()
      await db.entradas.delete(id)
        .then(() => {
          updateDate.updateLocalDate(date)
        });
      await controller.deleteEntrada(id)
        .then(() => {
          router.refresh()
          updateDate.updateRemoteDate(date)
        });
    }
  };

  return (
    <button onClick={removeTopic} className="text-red-400">
      <HiOutlineTrash size={24} />
    </button>
  );
}
