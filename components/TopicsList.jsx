"use client"
import { useCallback, useEffect, useState } from "react";
import Link from "next/link";
import RemoveBtn from "./RemoveBtn";
import { HiPencilAlt } from "react-icons/hi";
import getIdbData from "@/app/Idb";
import { db } from "@/configDB/indexedDb";

export default function TopicsList() {
  const [topics, setTopics] = useState([])

  const getEntradas = useCallback(async () => {
    let _entradas = await getIdbData();
    if (_entradas) {
      setTopics(_entradas)
    } else {
      _entradas = db.entradas.toArray()
      setTopics(_entradas)
    }
    if (Array.isArray(_entradas)) {
      setTopics(_entradas)
    }
  }, [])

  function compareDatabases() {
    let dateLocal = localStorage.getItem('update-local')
    if(dateLocal) {
      dateLocal = JSON.parse(dateLocal)
    }
    let dateRemote = localStorage.getItem('update-remote')
    if(dateRemote) {
      dateRemote = JSON.parse(dateRemote)
    }
    if(dateRemote < dateLocal){
      const _entradas = db.entradas.toArray()
      _entradas.forEach(async ()=>{
        
      })
    }
  }


  const onChangeSelect = e => {
    getEntradas(e.target.value)
  }

  useEffect(() => {
    getEntradas();
    compareDatabases();
  }, [getEntradas])

  return (
    <>
      <div className="flex w-full justify-between">
        <div className="flex items-center gap-2">
          <span>Ordenar por: </span>
          <select
            onChange={onChangeSelect}
          >
            <option value="" >Ninguno</option>
            <option value="autor" >Autor</option>
            <option value="titulo" >Título</option>
            <option value="contenido" >Contenido</option>
          </select>
        </div>

      </div>


      {topics.map((t) => (
        <div
          key={t.id}
          className="border border-slate-300 my-3 flex justify-between items-center rounded"
        >
          <Link href={`/${t.id}`} className="cursor-pointer hover:bg-gray-100 w-full p-4">
            <h2 className="font-bold text-xl">{t.titulo}</h2>
            <h3 className="text-md">
              {
                t.contenido.length > 70
                  ? t.contenido.substring(0, 70) + "..."
                  : t.contenido
              }
            </h3>
            <div className="flex flex-row justify-between">
              <h5 className="font-light text-sm">{t.autor}</h5>
              <h5 className="font-light text-sm">{t.publicacion.slice(0, 10)}</h5>
            </div>
          </Link>

          <div className="flex gap-2 p-4">
            <RemoveBtn id={t.id} />
            <Link href={`/form/${t.id}`}>
              <HiPencilAlt size={24} />
            </Link>
          </div>
        </div>
      ))}
    </>
  );
}
