function updateLocalDate(date) {
    localStorage.setItem('update-local', JSON.stringify(date))
}

function updateRemoteDate(date) {
    localStorage.setItem('update-remote', JSON.stringify(date))
}

const updateDate = {
    updateLocalDate,
    updateRemoteDate
}

export default updateDate