import axios from "axios"

const urlBase = process.env.url || "http://localhost:3000/"
const endpoint = urlBase + "/api/topics/"

function getEntradas(orderby = null) {
    return new Promise(async (resolve, reject) => {
        try {
            const { data } = await axios.get(endpoint, { params: { orderby } });
            resolve(data)
        } catch (error) {
            reject(error)
        }
    })
}

function getEntrada(id) {
    return new Promise(async (resolve, reject) => {
        try {
            const { data } = await axios.get(endpoint + id);
            resolve(data);
        } catch (error) {
            reject(error);
        }
    })
}

function createEntrada(payload) {
    return new Promise(async (resolve, reject) => {
        try {
            const { data } = await axios.post(endpoint, payload)
            resolve(data)
        } catch (error) {
            reject(error)
        }
    })
}

function updateEntrada(id, payload) {
    return new Promise(async (resolve, reject) => {
        try {
            const { data } = await axios.put(endpoint + id, payload);
            resolve(data);
        } catch (error) {
            reject(error);
        }
    })
}

function deleteEntrada(id) {
    return new Promise(async (resolve, reject) => {
        try {
            const { data } = await axios.delete(endpoint + id);
            resolve(data);
        } catch (error) {
            reject(error);
        }
    })
}

const controller = {
    getEntrada, getEntradas, createEntrada, updateEntrada, deleteEntrada
}
export default controller;