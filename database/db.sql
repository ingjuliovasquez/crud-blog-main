CREATE DATABASE blogtest;

use blogtest;

CREATE TABLE entrada (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    titulo TEXT NOT NULL,
    autor TEXT NOT NULL,
    contenido TEXT NOT NULL,
    publicacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

describe post_demo;