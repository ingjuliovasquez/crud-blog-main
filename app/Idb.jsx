"use client"
import controller from "@/database/controller"
import { db } from "@/configDB/indexedDb"

export default async function getIdbData() {
    try {
        const _entradas = await controller.getEntradas()
        if (_entradas) {
            _entradas.forEach(async (entrada) => {
                await db.entradas.put(entrada)
            })
        }
        return _entradas
    } catch (error) {
        console.error(error)
    }
}
